var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var users = [];
var id = 0;
const port = process.env.PORT  || 3000;

app.post('/:userId', authMiddleware, async (req, res, next) => {
  if(req.params.userId == 'post'){
    req.user.socket.emit('public', req.user.id, req.query.msg);
  }else{
    let recipient = users.find(u=>u.id == req.params.userId);
    req.user.socket.to(recipient.id).emit('pm', req.user.id, req.query.msg);
  }
  res.end();
});

app.get('/ping', async(req, res, next)=>{
  res.send('pong:'+(new Date()).toISOString());
});

async function authMiddleware(req, res, next) {
  let user = users.find(u=>u.id == req.headers.authorization);
  if(!user)
    return res.status(403).send('unauthorized');
  req.user = user;
  next();
}

io.on('connection', function (socket) {
  let user = {
    id: null,
    socket,
    socketId: socket.id
  };
  users.push(user);
  socket.on('register', (username)=>{
    let existingUserIndex = users.findIndex(u=>u.id == username);
    if(existingUserIndex > -1){
      let existingUser = users[existingUserIndex];
      existingUser.socket.disconnect(true);
      users.splice(existingUserIndex, 1);
    }
    socket.join(username);
    user.id = username;
    // users.push(user);
    socket.emit('connected', user.id);
    log(`user ${user.id} connected`);
  });
  
  socket.on('call', (username, sdp)=>{
    let recipient = users.find(u=>u.id == username);
    if(!recipient){
      log(`${userId} not found`);
    }else{
      log(user.id + ' is calling ' + username);
      socket.to(recipient.id).emit('remote_call', user.id, sdp);
    }
  });

  socket.on('accept_call', (username, sdp)=>{
    let recipient = users.find(u=>u.id == username);
    if(!recipient){
      log(`${userId} not found`);
    }else{
      log(user.id + ' accepted ' + username);
      socket.to(recipient.id).emit('remote_call_accepted', user.id, sdp);
    }
  });

  socket.on('ice', (username, icecandidates)=>{
    let recipient = users.find(u=>u.id == username);
    if(!recipient){
      log(`${userId} not found`);
    }else{
      socket.to(recipient.id).emit('ice_received', icecandidates);
    }
  });

  socket.on('pm_send', (userId, message)=>{
    let recipient = users.find(u=>u.id == userId);
    if(!recipient){
      log(`${userId} not found`);
    }else{
      log(`${user.id } to ${userId}: ${message}`);
      socket.to(recipient.id).emit('pm', user.id, message);
    }
  });

  socket.on('post', (message)=>{
    log(`${user.id } to public: ${message}`);
    io.sockets.emit('public', user.id, message);
  });

  socket.on('disconnect', function () {
    let i = users.findIndex(u=>u.id == user.id);
    users.splice(i, 1);
    log(`user ${user.id} disconnected`);
  });

  socket.on('ping_send', (timestamp, username) => {
    log(`${user.id} is pinging ${username || 'server'}`)
    if(username){
      let recipient = users.find(u=>u.id == username);
      if(!recipient){
        return log(`${userId} not found`);
      }
      socket.to(recipient.id).emit('ping_received', timestamp, user.id);
    }else{
      socket.emit('pong_received', new Date());
    }
  });

  socket.on('pong_send', (timestamp, username) => {
    if(username){
      let recipient = users.find(u=>u.id == username);
      if(!recipient){
        return log(`${userId} not found`);
      }
      socket.to(recipient.id).emit('pong_received', timestamp, user.id);
    }
  });

  socket.on('request', (timestamp, username) => {
    let recipient = users.find(u=>u.id == username);
    if(!recipient){
      return log(`${username} not found`);
    }
    socket.to(recipient.id).emit('request', timestamp, user.id);
    log(`${user.id} sent a request to ${username}`);
  });

  socket.on('response', (timestamp, username, response) => {
    let recipient = users.find(u=>u.id == username);
    if(!recipient){
      return log(`${username} not found`);
    }
    socket.to(recipient.id).emit('response', timestamp, user.id, response);
    log(`${user.id} responded '${response}' to a request from ${username}`);
  });
});

http.listen(port, function () {
  log('listening on *:' + port);
});

function log(message, ...args){
  let timestamp = new Date();
  console.log(timestamp.toISOString() + ' | ' + message, ...args);
}